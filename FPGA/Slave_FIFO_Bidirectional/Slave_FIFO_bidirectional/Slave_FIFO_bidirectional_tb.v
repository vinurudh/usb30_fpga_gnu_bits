`timescale 1ns/1ps

`define HALF_CLOCK_PERIOD 10
`define RESET_PERIOD 400
`define FLAGC_PERIOD 100
`define FLAGD_PERIOD 200
`define FLAGC_PERIOD1 140
`define FLAGD_PERIOD1 240
`define FLAGA_PERIOD 300
`define FLAGB_PERIOD 350
`define FLAGC_PERIOD2 2000
`define FLAGD_PERIOD2 2100
`define SIM_DURATION 200000



module Slave_FIFO_bidirectional_tb();
   //wire[31:0] data;
   //wire [31:0] datain_tb;
   //wire [31:0] tb_q;
	//wire [31:0] tb_test;
	wire clk_out; // 100 Mhz clock from PLL
	//wire clk_DDR_0_1; // DDR clock (clk_out)
	wire clk_pll_100;
	wire slwr;
	wire slrd;
	wire [1:0] faddr;
	wire sloe;
	wire slcs;
	wire pktend;
	wire [1:0] PMODE;
	wire RESET;
	wire [3:0] state_out;

// ### clock generation process ###

reg tb_local_clock = 0;


initial 
	begin : clock_generation_process
	tb_local_clock = 0;
		forever
		begin
			#`HALF_CLOCK_PERIOD tb_local_clock = ~tb_local_clock;
			end
	end
	
//### active low reset generation process ###

reg tb_local_reset = 1;
reg tb_local_flaga = 1;
reg tb_local_flagb = 1;
reg tb_local_flagc = 1;
reg tb_local_flagd = 1;
reg [31:0] testreg1= 0;
reg [31:0] testreg2 = 0;
wire [31:0] testbus;

//always@(*) 
//	begin : read_state
//	testbus <= (slrd == 1'b0)? testreg1:32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
//end

initial
	begin : read_process
	
	testreg1 = 32'dz;
    testreg2 = 32'dz;
    #350
    testreg1 = 32'd31;
    #1050
    testreg1 = 32'dz;
    //#500
    //testreg2 = 32'd0
    testreg2 = 32'dz;
	 //#100
	//testreg1 = 32'd0;
	end
	

 assign testbus = testreg1;
 assign testbus = testreg2;
 
initial
	begin : reset_generation_process
	$display ("Simulation starts .......");
	//testreg1 = 'z;
  // testreg2 = 'z;
  
	
	#`RESET_PERIOD tb_local_reset = 1'b0;
	#700
	#`FLAGC_PERIOD tb_local_flagc = 1'b0;
	#100 tb_local_flagd = 1'b0;
//	#`FLAGC_PERIOD1 tb_local_flagc = 1'b0;
//	#`FLAGD_PERIOD1 tb_local_flagd = 1'b0;
//	//#`FLAGA_PERIOD tb_local_flaga = 1'b1;
//	//#`FLAGB_PERIOD tb_local_flagb = 1'b1;
//	//testreg1 = 44;
//	#`FLAGC_PERIOD2 tb_local_flagc = 1'b1;
//	#`FLAGD_PERIOD2 tb_local_flagd = 1'b1;	
//	
////	#`FLAGD_PERIOD1 tb_data_in = 32'b11111;
//	//#`FLAGA_PERIOD tb_data_in = 1'b0;
//
//	#`FLAGC_PERIOD tb_local_flagc = 1'b0;
//	#`FLAGD_PERIOD tb_local_flagd = 1'b0;
//	//testreg2 = 99;
//	
//	
//	#`FLAGC_PERIOD2 tb_local_flagc = 1'b1;
//	#`FLAGD_PERIOD2 tb_local_flagd = 1'b1;	
//	// testreg1 = 32'b11111;
//	#`FLAGC_PERIOD tb_local_flagc = 1'b0;
//	#`FLAGD_PERIOD tb_local_flagd = 1'b0;
	
	// #100
	//testreg2 = 47;
	//#100
	//testreg1 = 123;
	//#10
	//testreg1 = 'z;
   //testreg2 = 'z;

// #`FLAGC_PERIOD2 tb_local_flagc = 1'b1;
//	#`FLAGD_PERIOD2 tb_local_flagd = 1'b1;	
	//testreg1 = 32'b0;
	
//`define FLAGD_PERIOD 200
	#`SIM_DURATION
	$stop();
	end
	
	


Slave_FIFO_bidirectional  fpga_FX3_inout_inst_0( .reset_in(tb_local_reset),
										.clk_in(tb_local_clock),
										.data_inout(testbus),
										//.datain_tb(tb_data_in),
										//.data_gen_stream_test(tb_test),
										.addr(faddr),
										.slrd(slrd),
										.slwr(slwr),
										.flaga(tb_local_flaga),
										.flagb(tb_local_flagb),
										.flagc(tb_local_flagc),
										.flagd(tb_local_flagd),
										.sloe(sloe),
										.clk_out(clk_out), // 100 Mhz clock from PLL fed to Fx3
										//.clk_pll_100(clk_pll_100),
										/* This is the change made in verilog code (and where ever it was clk_100 now it should be clk_out
										altpll_fx3 inst_altpll_fx3
										(
										.refclk(clk),
										.rst(reset_in_), 
										.outclk_0(clk_out),
										.locked(lock)
										);*/
										//.clk_DDR_0_1(clk_DDR_0_1), //// DDR clock (clk_out) just for test purpose(This was previously fed to Fx3 now Fx3 is fed by clk_out
										.slcs(slcs),
										.pktend(pktend),
										.PMODE(PMODE),
										.RESET(RESET),
										.state_out(state_out)
										);
endmodule