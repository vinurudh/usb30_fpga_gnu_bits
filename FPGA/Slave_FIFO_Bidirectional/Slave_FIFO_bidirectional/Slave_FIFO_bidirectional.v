module Slave_FIFO_bidirectional#(parameter DATA_WIDTH = 16, parameter ADDR_WIDTH = 8)(
	input  flaga,               //empty flag from FX3 to initiate write operation 
	input  flagb, 				//empty partial flag from FX3 to initiate write operation
	input  flagc,               //full flag from FX3 to initiate read operation
	input  flagd,               //full flag from FX3 to initiate read operation
	output slrd,                //output read select
	output slwr,                //output write select
	output sloe,                //output output enable select
	output slcs,                //output chip select
	output pktend,              //output pkt end	
	input  reset_in,           //input reset active low
	output RESET,					 //output reset
	input  clk_in,                 //input clock 50 Mhz
	output clk_out,             //output clk 100 Mhz 
	output clk_pll_200,          //output clk 100 Mhz and 180 phase shift
	inout [31:0]data_inout,          //data bus
//input [31:0]datain_tb,
	output [1:0]addr,            //output 2-bit fifo address  
   output [1:0]PMODE,
	//output [3:0] state_out,  //LEDR 9 8 7 6  //*****used for DEBUG *****//
	//output enable,           //LEDR //*****used for DEBUG *****//
	output initiate
); 


reg [1:0] watermark_delay_cnt;	
reg rd_watermark_delay_cnt;


reg [31:0]data_out;
reg [31:0]data_out_d;
reg [31:0] data_in;
//reg [3:0] state_in;   //*****used for DEBUG *****//

reg slrd_delay1;
reg slrd_delay2;
reg slrd_delay3;
reg slrd_delay4;
reg [1:0]fifo_address;
reg [1:0]fifo_address_d;
reg flaga_d;
reg flagb_d;
reg flagc_d;
reg flagd_d;
wire slrd_mode;
wire sloe_mode;
reg initiate_write;

reg [3:0]current_state;
reg [3:0]next_state;
reg slwr_delay1;

//(* syn_encoding = “user" *) reg [3:0] current_state; //Not working
//(* syn_encoding = “user" *) reg [3:0] next_state; //Not working


//parameters for bidirectional mode state machine (Gray code pattern used to avoid state decoding glitches)
parameter [3:0] idle_state                    = 4'b0000; // 0
parameter [3:0] flagc_rcvd_state              = 4'b0001; // 1
parameter [3:0] wait_flagd_state              = 4'b0011;//3
parameter [3:0] read_state                    = 4'b0010;//2
parameter [3:0] read_watermark_delay_state    = 4'b0110;//6
parameter [3:0] wait_watermark_delay_state    = 4'b0111;//7
//parameter [3:0] check_for_write_state         = 4'd6;
parameter [3:0] wait_flaga_state              = 4'b0101;//5
parameter [3:0] wait_flagb_state              = 4'b0100;//4
parameter [3:0] write_state                   = 4'b1100;//12
parameter [3:0] write_wr_delay_state          = 4'b1101;//13

//output signal assignment

assign slrd   = slrd_mode;
assign slwr   = slwr_delay1;   
assign addr  = fifo_address_d;
assign sloe   = sloe_mode;
assign PMODE  = 2'b11;		
assign RESET  = 1'b1;	
assign slcs   = 1'b0;
assign pktend = 1'b1;

// Declare the ROM variable
reg [DATA_WIDTH-1:0] rom[0:2**ADDR_WIDTH-1];

reg [8:0] addr_sine;

initial
  begin
		$readmemh("sine_wave.txt", rom);
  end
  
 
//PLL to generate 100 MHz clock from the FPGA internal 50Mhz clock
Slave_FIFO_FX3_PLL  inst_clk_pll(
		.refclk(clk_in),   //  refclk.clk
		.rst(1'b0),      //   reset.reset
		.outclk_0(clk_out), // outclk0.clk
		.outclk_1 (clk_pll_200),
		.locked(lock)   //  locked.export
	);


assign reset_ = lock; 

//reading sine wave address 
  
 always@(posedge clk_out, negedge reset_) begin
    if(!reset_) begin
		addr_sine<= 8'd0;
	end
	 else if(slwr_delay1 == 1'b0) begin
	   addr_sine<= (addr_sine == 8'd255)? 8'd0:(addr_sine + 1'b1);
	end
	end



///flopping the INPUTs flags
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		flaga_d <= 1'd0;
		flagb_d <= 1'd0;
		flagc_d <= 1'd0;
		flagd_d <= 1'd0;
	end else begin
		flaga_d <= flaga;
		flagb_d <= flagb;
		flagc_d <= flagc;
		flagd_d <= flagd;
	end	
end

// output control signal generation based on the state machine
assign slrd_mode      = ((current_state == read_state) | (current_state == read_watermark_delay_state)) ? 1'b0 : 1'b1;
assign sloe_mode      = ((current_state == read_state) | (current_state == read_watermark_delay_state) | (current_state == wait_watermark_delay_state)) ? 1'b0 : 1'b1; 
assign slwr_mode      = ((current_state == write_state)) ? 1'b0 : 1'b1;

//delay for writing into slave fifo
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		slwr_delay1 <= 1'b1;
	end else begin
		slwr_delay1 <=  slwr_mode;
	end	
end


//delay for reading from slave fifo(data will be available after two clk_in cycle) 
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		slrd_delay1 <= 1'b1;
		slrd_delay2 <= 1'b1;  
		slrd_delay3 <= 1'b1;
		slrd_delay4 <= 1'b1;
 	end else begin
 		slrd_delay1 <= slrd_mode;
		slrd_delay2 <= slrd_delay1; 
		slrd_delay3 <= slrd_delay2;
		slrd_delay4 <= slrd_delay3;
	end	
end

// getting the input data
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		data_in <= 32'd0;
 	end else begin
		data_in <= data_inout;
	end	
end

///slave fifo address
always@(*)begin
	if((current_state == flagc_rcvd_state) | (current_state == wait_flagd_state) | (current_state == read_state) | (current_state == read_watermark_delay_state) | (current_state == wait_watermark_delay_state))begin
		fifo_address = 2'b11;
	end else 
		fifo_address = 2'b00;
end	

//flopping the output fifo address
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		fifo_address_d <= 2'd0;
 	end else begin
		fifo_address_d <= fifo_address;
	end	
end

//counter to delay the read because of watermark 
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		rd_watermark_delay_cnt <= 1'b0;
	end else if(current_state == read_state) begin
		rd_watermark_delay_cnt <= 1'b1;
        end else if((current_state == read_watermark_delay_state) & (rd_watermark_delay_cnt > 1'b0))begin
		rd_watermark_delay_cnt <= rd_watermark_delay_cnt - 1'b1;
	end else begin
		rd_watermark_delay_cnt <= rd_watermark_delay_cnt;
	end	
end

//Counter to delay for watermark time
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		watermark_delay_cnt <= 2'd0;
	end else if(current_state == read_watermark_delay_state) begin
		watermark_delay_cnt <= 2'd2;
        end else if((current_state == wait_watermark_delay_state) & (watermark_delay_cnt > 1'b0))begin
		watermark_delay_cnt <= watermark_delay_cnt - 1'b1;
	end else begin
		watermark_delay_cnt <= watermark_delay_cnt;
	end	
end


always @(posedge clk_out)begin
		if(data_inout & 32'd65535 == 32'd31)	
			begin
				initiate_write <= 1'b1;
			end 
		end
//bidirectional mode state machine 

 always @(posedge clk_out)
    begin
        if (clk_out) begin
            current_state <= next_state;
        end
    end

    always @(current_state or reset_ or flagc_d or flagd_d or rd_watermark_delay_cnt or watermark_delay_cnt or initiate_write or flaga_d or flagb_d)
    begin
        if (!reset_) begin
            next_state <= idle_state;
        end
        else begin
            case (current_state)
				 idle_state: begin
                    if ((flagc_d == 1'b1))
                        next_state <= flagc_rcvd_state;
                    else if ((flagc_d == 1'b0))
                        next_state <= idle_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= idle_state;
                end
                flagc_rcvd_state: begin
                    next_state <= wait_flagd_state;
                end
                wait_flagd_state: begin
                    if ((flagd_d == 1'b1))
                        next_state <= read_state;
                    else if ((flagd_d == 1'b0))
                        next_state <= wait_flagd_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= wait_flagd_state;
                end
                read_state: begin
                    if ((flagd_d == 1'b0))
                        next_state <= read_watermark_delay_state;
                    else if ((flagd_d == 1'b1))
                        next_state <= read_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= read_state;
                end
                read_watermark_delay_state: begin
                    if ((rd_watermark_delay_cnt == 1'b0))
                        next_state <= wait_watermark_delay_state;
                    else if ((rd_watermark_delay_cnt != 1'b0))
                        next_state <= read_watermark_delay_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= read_watermark_delay_state;
                end
                wait_watermark_delay_state: begin
                    if (((watermark_delay_cnt == 1'b0) & (initiate_write == 1'b1)))
                        next_state <= wait_flaga_state;
                    else if (((watermark_delay_cnt != 1'b0) | (initiate_write != 1'b1)))
                        next_state <= wait_watermark_delay_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= wait_watermark_delay_state;
                end
                wait_flaga_state: begin
                    if ((flaga_d == 1'b1))
                        next_state <= wait_flagb_state;
                    else if ((flaga_d != 1'b1))
                        next_state <= wait_flaga_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= wait_flaga_state;
                end
					 wait_flagb_state: begin
                    if ((flagb_d != 1'b1))
                        next_state <= wait_flagb_state;
                    else if ((flagb_d == 1'b1))
                        next_state <= write_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= wait_flagb_state;
                end
                write_state: begin
                    if ((flagc_d == 1'b1))
                        next_state <= flagc_rcvd_state;
                    else if (((flagb_d == 1'b0) & (flagc_d == 1'b0)))
                        next_state <= write_wr_delay_state;
                    else if (((flagc_d == 1'b0) & (flagb_d == 1'b1)))
                        next_state <= write_state;
                    // Inserting 'else' block to prevent latch inference
                    else
                        next_state <= write_state;
                end
                write_wr_delay_state: begin
                    next_state <= idle_state;
                end
                default: begin
                    $display ("Reach undefined state");
                end
            endcase
        end
    end
// statemachine


//data generator counter for StreamIN modes
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		data_out_d <= 32'd0;
	end else if(slwr_delay1 == 1'b0) begin
		data_out_d <= rom[addr_sine];
	end 
end	

always @(posedge clk_out, negedge reset_)
begin
	if(!reset_)begin 
		data_out <= 32'd0;
	end 
	else if (slrd_delay4==1'b0) begin
		data_out <= data_inout;
	end 
	else if(slwr_delay1==1'b0) begin
		data_out <= data_out_d;
	end 
end

assign data_inout = (slwr_delay1) ? 32'dz : data_out;

assign initiate = initiate_write;
//assign state_out = state_in; //*****used for DEBUG *****//
//assign enable= (data_inout== 32'd31)? 1: 0; //*****used for DEBUG *****//
endmodule
