FPGA Readme

1)The folder Slave_FIFO_Write contains the projects that perform stream_in operations to the Fx3 slave FIFO. 
    sub folders:
    
    --> Slave_FIFO_Write: This quartus FPGA projects include the implementation of the writing the counter values to the Fx3 slave FIFO
    
    --> Slave_FIFO_Write_sine_wave: This is the extension of above implementation here the sinusoidal value is written to the Fx3 slave fifo(Slave_FIFO_write.v) 
     This project also contains the respective test bench(slave_FIFO_Write_tb.v) that used to perform functional simulation
     
2) The folder Slave_FIFO_Read contains the project that performs the stream_out operation from the FX3 slave FIFO. To see the values read from FX3 on FPGA, we have assigned the data values to red and green LEDs on the FPGA board

3) The folder Slave_FIFO_bidirectional contains the implementation of bi-directional transmission of data between USB host and the FPGA using FX3 slave 
    --> Slave_FIFO_bidirectional_base folder contains the initial implementation of bi-directional transmission.Here it was observed in SignalTap simulation that, the control path state machine is stuck in idle state and transmission is halted after one cycle of data transmitted (size of all the slave FIFO buffers that are configured for DMA producer to consumer socket) by FPGA to the USB has been successfully finished. 
    
    --> Slave_FIFO_bidirectional (version 1): The DMA flags that are configured for producer to consumer(FPGA to USB), flaga and flagb are disabled(HIGH state)once all the buffers of P2U sockets are filled and interrupting the data transmission, Thus in idle state a condition initiate_write is checked each time if the transmission to be performed without any interruption.
    
    --> Slave_FIFO_bidirectional (version 2):
    Two problems observed in the control path state machine implementation in the project version 1.
        i) In netlist viewers -> state machine viewer, it is noted that the control path state machine is not synthesised properly. 
        ii) During SignalTap simulation few state flips are observed in control path state machine.
    To solve the above problems, the state machine has been redesigned with state parameters with Gray code values, and an else condition is added in each state to prevent latch inference.