module Slave_FIFO_Read(
	input  reset_in_,            //input reset active low
	input  clk,                  //input clock 50 Mhz
	input  [31:0]fdata,          //32 bit data bus
	output [1:0]faddr,           //output 2 bit fifo address  
	output slrd,                 //output read select
	output slwr,                 //output write select
	input  flaga,                //full flag
	input  flagb,                //partial full flag
   input  flagc,                //empty flag
	input  flagd,                //empty partial flag
	output sloe,                 //output output enable select
	output clk_out,              //output clk 100 Mhz
	output slcs,                 //output chip select
	output pktend,               //output pkt end
	output [1:0]PMODE,
	output RESET,
	 ///////// LEDG /////////
	output      [7:0]  LEDG,

      ///////// LEDR /////////
   output      [9:0]  LEDR
);


reg [2:0]current_state;
reg [2:0]next_state;

//parameters for FPGA Read mode state machine
parameter [2:0] idle_state                   = 3'd0;
parameter [2:0] flagc_rcvd_state             = 3'd1;
parameter [2:0] wait_flagd_state             = 3'd2;
parameter [2:0] read_state                   = 3'd3;
parameter [2:0] read_watermark_delay_state   = 3'd4;
parameter [2:0] wait_watermark_delay_state   = 3'd5;

reg [1:0] wait_watermark_cnt;	
reg read_watermark_cnt; 
reg  flaga_d;
reg  flagb_d;
reg  flagc_d;
reg  flagd_d;

wire slrd_FIFO_READ;
wire sloe_FIFO_READ;

wire [31:0]data_in_FIFO_READ;

//output signal assignment (not relevant signals are fed with default values)
assign slrd   = slrd_FIFO_READ;
assign slwr   = 1'b1;   
assign faddr  = 2'b11;
assign sloe   = sloe_FIFO_READ;
assign data_in_FIFO_READ = fdata;
assign PMODE  = 2'b11;		
assign RESET  = 1'b1;	
assign slcs   = 1'b0;
assign pktend = 1'b1;

//assign the starting 18 bits to the LEDS available on the FPGA board
assign LEDG = fdata[7:0];
assign LEDR = fdata[17:8];


wire lock;
wire reset_;

//PLL to genearate output clock of 100Mhz
Slave_FIFO_FX3_PLL inst_clk_pll
	(
		.refclk(clk),
		.rst(1'b0/*reset2pll*/), 
		.outclk_0(clk_out), //output clock 100 Mhz
		.locked(lock)
	);


assign reset_ = lock;	

//control ouput signals
assign slrd_FIFO_READ = ((current_state == read_state) | (current_state == read_watermark_delay_state)) ? 1'b0 : 1'b1;
assign sloe_FIFO_READ = ((current_state == read_state) | (current_state == read_watermark_delay_state) | (current_state == wait_watermark_delay_state)) ? 1'b0 : 1'b1;

///reading the DMA buffer relavant flags
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		flaga_d <= 1'd0;
		flagb_d <= 1'd0;
		flagc_d <= 1'd0;
		flagd_d <= 1'd0;
	end else begin
		flaga_d <= flaga;
		flagb_d <= flagb;
		flagc_d <= flagc;
		flagd_d <= flagd;
	end	
end


//counter to delay the read and output enable signal
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		read_watermark_cnt <= 1'b0;
	end else if(current_state == read_state) begin
		read_watermark_cnt <= 1'b1;
        end else if((current_state == read_watermark_delay_state) & (read_watermark_cnt > 1'b0))begin
		read_watermark_cnt <= read_watermark_cnt - 1'b1;
	end else begin
		read_watermark_cnt <= read_watermark_cnt;
	end	
end

//Counter to delay the OUTPUT Enable(oe) signal
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		wait_watermark_cnt <= 2'd0;
	end else if(current_state == read_watermark_delay_state) begin
		wait_watermark_cnt <= 2'd2;
        end else if((current_state == wait_watermark_delay_state) & (wait_watermark_cnt > 1'b0))begin
		wait_watermark_cnt <= wait_watermark_cnt - 1'b1;
	end else begin
		wait_watermark_cnt <= wait_watermark_cnt;
	end	
end

//Slave FIFO read mode state machine
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		current_state <= idle_state;
	end else begin
		current_state <= next_state;
	end	
end

//Slave FIFO read mode state machine combinational logic
always @(*)begin
	next_state = current_state;
	case(current_state)
	idle_state:begin
		if(flagc_d == 1'b1)begin
			next_state = flagc_rcvd_state;
		end else begin
			next_state = idle_state;
		end
	end	
        flagc_rcvd_state:begin
		next_state = wait_flagd_state;
	end	
	wait_flagd_state:begin
		if(flagd_d == 1'b1)begin
			next_state = read_state;
		end else begin
			next_state = wait_flagd_state;
		end
	end	
        read_state :begin
                if(flagd_d == 1'b0)begin
			next_state = read_watermark_delay_state;
		end else begin
			next_state = read_state;
		end
	end
	read_watermark_delay_state : begin
		if(read_watermark_cnt == 0)begin
			next_state = wait_watermark_delay_state;
		end else begin
			next_state = read_watermark_delay_state;
		end
	end
        wait_watermark_delay_state : begin
		if(wait_watermark_cnt == 0)begin
			next_state = idle_state;
		end else begin
			next_state = wait_watermark_delay_state;
		end
	end
	endcase
end

endmodule
