# USB Driver - c++

The FX3 is interfaced to the PC. The USB driver is developed in the Windows.

## Getting Started

The project is developed on the Visual Studio 2015. Having the same version shall help alot but the project content can be run in the eclipse with some small modifications. 

### Prerequisites

For the USB driver to work in windows we need Cypress USB libraries. This shall ease the task and let you interact with the cypress device with ease. The library used is CyAPI.lib. All the includes that need to be added in the project are available in the folder libraries.

## Deployment

Connect the Device, open the project and start the application. It will continously keep printing the data recieved. 

