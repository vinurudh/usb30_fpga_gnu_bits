
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::usb_source::my_usbsource "<+description of block+>"

%feature("docstring") gr::usb_source::my_usbsource::make "Return a shared_ptr to a new instance of usb_source::my_usbsource.

To avoid accidental use of raw pointers, usb_source::my_usbsource's constructor is in a private implementation class. usb_source::my_usbsource::make is the public interface for creating new instances.

Params: (NONE)"