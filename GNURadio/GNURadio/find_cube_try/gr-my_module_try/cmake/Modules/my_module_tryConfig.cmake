INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_MY_MODULE_TRY my_module_try)

FIND_PATH(
    MY_MODULE_TRY_INCLUDE_DIRS
    NAMES my_module_try/api.h
    HINTS $ENV{MY_MODULE_TRY_DIR}/include
        ${PC_MY_MODULE_TRY_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    MY_MODULE_TRY_LIBRARIES
    NAMES gnuradio-my_module_try
    HINTS $ENV{MY_MODULE_TRY_DIR}/lib
        ${PC_MY_MODULE_TRY_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MY_MODULE_TRY DEFAULT_MSG MY_MODULE_TRY_LIBRARIES MY_MODULE_TRY_INCLUDE_DIRS)
MARK_AS_ADVANCED(MY_MODULE_TRY_LIBRARIES MY_MODULE_TRY_INCLUDE_DIRS)

