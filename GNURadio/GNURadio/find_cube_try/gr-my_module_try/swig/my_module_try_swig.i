/* -*- c++ -*- */

#define MY_MODULE_TRY_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "my_module_try_swig_doc.i"

%{
#include "my_module_try/find_cube.h"
%}


%include "my_module_try/find_cube.h"
GR_SWIG_BLOCK_MAGIC2(my_module_try, find_cube);
