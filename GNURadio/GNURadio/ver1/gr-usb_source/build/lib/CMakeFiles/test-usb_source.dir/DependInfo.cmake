# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vivek/code/gr-usb_source/lib/qa_my_usbsource.cc" "/home/vivek/code/gr-usb_source/build/lib/CMakeFiles/test-usb_source.dir/qa_my_usbsource.cc.o"
  "/home/vivek/code/gr-usb_source/lib/qa_usb_source.cc" "/home/vivek/code/gr-usb_source/build/lib/CMakeFiles/test-usb_source.dir/qa_usb_source.cc.o"
  "/home/vivek/code/gr-usb_source/lib/test_usb_source.cc" "/home/vivek/code/gr-usb_source/build/lib/CMakeFiles/test-usb_source.dir/test_usb_source.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/vivek/code/gr-usb_source/build/lib/CMakeFiles/gnuradio-usb_source.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
