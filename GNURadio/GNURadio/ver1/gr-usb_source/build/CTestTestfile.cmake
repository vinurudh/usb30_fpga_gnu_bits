# CMake generated Testfile for 
# Source directory: /home/vivek/code/gr-usb_source
# Build directory: /home/vivek/code/gr-usb_source/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/usb_source)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)
