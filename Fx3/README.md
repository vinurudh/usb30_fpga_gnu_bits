# FX3 Development

The complete FX3 implementation is presented here.

## Getting Started

The FX3 is divided into two parts for FX3.
 * SourceCode : This contains the firmware / application for the FX3.
 * GPIF II Project : This contains the GPIF statemachine handling
 * doxygen : This contains the doxygen documentation for the FX3 application situated in [SourceCode] (https://bitbucket.org/usb30gnu/usb30_fpga_gnu/src/81f19b5a3cda53e37e796bb7c01f2dd086025579/Fx3/SourceCode/Fx3/?at=master)

The FX3 application / firmware is developed using the EZ USB Suite provided from Cypress (customized on Eclipse). The state machine is developed using the GPIF Designer. The statemachine developed is included as a header file in the FX3 aaplicaiton.
 
 
## References

 * [Cypress FX3 Website] (http://www.cypress.com/products/ez-usb-fx3-superspeed-usb-30-peripheral-controller)
 * [SuperSpeed Device Design by Example, 2nd Edition - John Hyde] (http://www.cypress.com/documentation/other-resources/superspeed-device-design-example-john-hyde)

