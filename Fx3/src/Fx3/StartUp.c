/**
 * @file	main.cpp
 * @brief	The <b> main() </b> resides here... <br>
 * 			Instantiates the basic settings of Fx3 and calls the kernel
 *
 * @author	Vinurudh Damarla
 *
 */

#include "Application.h"

/**
 * @brief <b> main function </b><br>
 * Sets up the CPU environment the starts the RTOS
 *
 * @return int, status OK
 */
int main (void)
{
    CyU3PIoMatrixConfig_t io_cfg;
    CyU3PReturnStatus_t Status;
    CyU3PSysClockConfig_t ClockConfig;

    // Clock setting
    ClockConfig.setSysClk400  = CyTrue;
    ClockConfig.cpuClkDiv     = 2;
    ClockConfig.dmaClkDiv     = 2;
    ClockConfig.mmioClkDiv    = 2;
    ClockConfig.useStandbyClk = CyFalse;
    ClockConfig.clkSrc        = CY_U3P_SYS_CLK;
    Status = CyU3PDeviceInit(&ClockConfig);


   if (Status == CY_U3P_SUCCESS)
    {
		Status = CyU3PDeviceCacheControl(CyTrue, CyTrue, CyTrue);
		if (Status == CY_U3P_SUCCESS)
		{
			CyU3PMemSet((uint8_t *)&io_cfg, 0, sizeof(io_cfg));
			io_cfg.isDQ32Bit = CyTrue;
			io_cfg.useI2C    = CyFalse;
			io_cfg.useI2S    = CyFalse;
			io_cfg.useSpi    = CyFalse;
			io_cfg.useUart   = CyTrue;
			io_cfg.gpioSimpleEn[0] = 0;
			io_cfg.gpioSimpleEn[1] = 1<<(45-32);
			io_cfg.lppMode   = CY_U3P_IO_MATRIX_LPP_DEFAULT;
			Status = CyU3PDeviceConfigureIOMatrix(&io_cfg);
			if (Status == CY_U3P_SUCCESS) CyU3PKernelEntry();
		}
	}
    while (1);
	return 0;
}


